# Click on this function definition to see its references
def add(x ,y):
    return x + y


if __name__ == "__main__":
    # Click on these function calls to see their definition
    add(42, 1)
    add(42, 2)
