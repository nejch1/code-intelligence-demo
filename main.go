package main

import "fmt"


// Hover over this function definition to see its references
func add(x int, y int) int {
	return x + y
}

func main() {
	// Hover over these function calls to see their definition
	fmt.Println(add(42, 1))
	fmt.Println(add(42, 2))
}
